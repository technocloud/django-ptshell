# django-ptshell

## Requirements

- Python 3.10+
- Django 3.2-4.2

## Installation

### Add repository to pyproject.toml

```
[[tool.poetry.source]]
name = "django-ptshell"
url = "https://gitlab.com/api/v4/projects/46380420/packages/pypi/simple"
default = false
secondary = true
```

### Install package

```
poetry add --source django-ptshell django-ptshell
```

### Add to django settings.py

```
INSTALLED_APPS = [
    ...
    # Third-party apps
    "django_ptshell",
    ...
]
```

### Add to .gitignore and .dockerignore

```
...
/.ptpython_history
```

## Usage

```
./manage.py shell
```

## TODO List

- [ ] Import all django models (the same as shell_plus)
- [ ] Configuration ptpython theme and other confs via django settings

## CHANGELOG

- <em>(2023-05-28)</em> **0.1.0**: First release
