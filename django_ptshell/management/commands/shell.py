from pathlib import Path

from django.core.management import BaseCommand
from ptpython.layout import CompletionVisualisation
from ptpython.repl import embed


def ptpython_configure(repl):
    repl.completion_visualisation = CompletionVisualisation.MULTI_COLUMN
    repl.use_code_colorscheme("nord-darker")


class Command(BaseCommand):
    help = "Shell with ptpython"

    def handle(self, *args, **options):
        history_filename = Path.cwd() / ".ptpython_history"
        embed(globals=globals(), history_filename=history_filename, configure=ptpython_configure)
