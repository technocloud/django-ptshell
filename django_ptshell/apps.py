from django.apps import AppConfig


class PtShellConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "django_ptshell"
